import React, { Component } from 'react';
import axios from 'axios';
import Moment from 'react-moment';

import config from './../config.js';
const apiURL = config.apiURL;

class News extends Component {
  constructor(props) {
    super(props);
    this.intervalId = null;
    this.state = {
      newsLoaded: false,
      news: 'Loading...',
      err: null
    };
  }

  getNews = () => {
    const data = {
      sources: this.props.sources,
      keywords: this.props.keywords,
      category: this.props.category,
      language: this.props.language,
      country: this.props.country
    };
    axios.post(`${apiURL}/news`, data)
    .then(res => {
      console.log(res);
      if(res.status === 200 && res.data.status === 200) {
        this.setState({
					newsLoaded: true,
					news: res.data.news,
				});
      }
      else {
        this.setState({
          newsLoaded: true,
          news: 'Service currently unavailable',
          err: res.err
        })
      }
    }).catch(err => {
      this.setState({
        newsLoaded: false,
        news: 'Service currently unavailable',
        err: err
      })
      console.log(err);
    });
  }

  componentDidMount() {
    this.getNews();
    this.intervalId = setInterval(this.getNews.bind(this), this.props.refreshInterval * 1000);
	}

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  render() {
    if(this.state.newsLoaded && this.state.err === null) {
			return(
        <div id="News-wrapper">
        <ul>
          {	this.state.news.map((article, index) => {
            if(index < 5) {
              return(
                <li key={article.url}>
                  <h3>{article.title} - {article.author}</h3>
                  <Moment format="HH:mm ">{article.time}</Moment>
                </li>
              )
            }
            else {return('')}
          })
          }
          </ul>
        </div>
			)
		}
    else {
      return(
        <div id="News-wrapper">
					{this.state.news}<i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
				</div>
      )
    }
	}
}

export default News
