
import React, { Component } from 'react';
import axios from 'axios';
import './../public/css/alarm-clock.css';
//import Moment from 'react-moment';
//import socketIOClient from 'socket.io-client';
import Sound from 'react-sound';

import config from './../config.js';
const apiURL = config.apiURL;
const apiDomain = config.apiDomain;
const streamURL = config.mjpg;
const timeout = 2500; //2.5s
let alarmCounter = 0;

class AlarmClock extends Component {
	constructor(props) {
		super(props);
    this.showStream = false;
		this.state = {
			kairosSubjectID: this.props.kairosSubjectID,
			kairosGalleryName: this.props.kairosGalleryName,
			identityMatch: '',
			soundURL: "https://freesound.org/data/previews/417/417162_8111977-lq.mp3",
			alarm: '',
			playStatus: Sound.status.STOPPED,
			volume: 100,
			autoLoadSound: true,
			camHeight: 300,
			camWidth: 300
		};
	}

	updateAlarm = () => {
		// axios.get(`${apiDomain}/update-alarm`)
		// 	.then(res => {
		// 		//console.log("Identity: " + res.data.response);
		//    }, err => {
		//      console.log(" " + err);
		// });
	}

	componentWillMount(){
		window.soundManager.setup({debugMode:false});
	}

	componentDidMount() {
    //start streaming
    axios.get(`${apiDomain}/raspberry/stream/start`)
      .then(res => {
		  this.showStream = true;
        if(res.status === 200) {
          
        }
      })
		this.updateAlarm();
		// const socket = socketIOClient(apiDomain);
		// socket.on('update alarm', () => {
		// 	console.log("update alarm emit received!");
		// 	this.updateAlarm();
		// })
	}

	capture = () => {

	};

	createIdentity = () => {
		let reqData = {
			img: this.capture(),
			subjectID: this.state.kairosSubjectID,
			galleryName: this.state.kairosGalleryName,
			action: 'enroll'
		};
		axios.post(`${apiURL}/face-recognition/identity`, reqData)
			.then(res => {
				console.log("Identity created: " + JSON.stringify(res.data.response));
			 }, err => {
				 console.log("createIdentity error: " + err);
		});
	};

verifyIdentity = () => {
	return new Promise(resolve => {
		let reqData = {
			img: this.capture(),
			subjectID: this.state.kairosSubjectID,
			galleryName: this.state.kairosGalleryName,
			action: 'recognize'
			//action: 'verify'
		};
		console.log((new TextEncoder('utf-8').encode(reqData.img)).length)
		//console.log("size " + Buffer.bytesLength(reqData.img, 'utf-8'));
		axios.post(`${apiURL}/face-recognition/identity`, reqData)
			.then(res => {
				console.log((res));
				resolve(res.data.response);
	});
});
}

	checkAlarm = async() => {
		console.log("called checkAlarm");
		let alarmStopped = await this.stopAlarm();
		if(!alarmStopped && alarmCounter < 25) {
			console.log("still on");
			alarmCounter++;
			setTimeout(() => this.checkAlarm(), timeout);
		}
		else{
			//turn off alarm
			alarmCounter = 0;
			this.setState({alarm: false});
		}
	}

	startAlarm = () => {
		//socket.io to trigger ?
		this.setState({
			alarm: true,
			playStatus: Sound.status.PLAYING
		});
		this.checkAlarm();
	};

	stopAlarm = async() => {
		let identity = await this.verifyIdentity();
    console.log(identity);
		if(typeof identity.Errors === 'undefined') {
			const match = Math.round(identity.images[0].transaction.confidence * 100);
			console.log("Identity match: " + match + "%");
			if(match > 60) {
				this.setState({
					identityMatch: `Identity match: ${match}%`,
					playStatus: Sound.status.STOPPED,
					alarm: false
				});
				return true;
			}
			else {
				return false;
			}
		}
		else{
			this.setState({identityMatch: `Identity verification failed. Pls try again`});
			return false;
		}
	};

	handleSongFinishedPlaying = () => {
		//if alarm on - repeat song
		if(this.state.alarm) {
			this.setState({playStatus: Sound.status.PLAYING});
		}
	}

	render() {
		return(
			<div id="AlarmClock-wrapper">
				<div id="AlarmClock-camera">
				<img width="300" height="300" src="http://localhost:8000/stream.mjpg" alt="" style={{borderRadius:'50%!important'}}/>
			</div>
				<Sound
					autoLoad={this.state.autoLoadSound}
					url={this.state.soundURL}
					playStatus={this.state.playStatus}
					volume={this.state.volume}
					onFinishedPlaying={this.handleSongFinishedPlaying}
				/>
				<div id="AlarmClock-controls">
					<button onClick={this.createIdentity}>Create Identity</button>
					<button onClick={this.startAlarm}>Start alarm</button>
					<button onClick={this.stopAlarm}>Stop alarm</button>
					<br />{this.state.identityMatch}
				</div>
			</div>
		)
	}
}

export default AlarmClock;
