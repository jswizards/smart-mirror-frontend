import React, { Component } from 'react';
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import './../public/css/calendar.min.css';

class Calendar extends Component {
	render() {
		return(
			<div id="Calendar-wrapper">
			<DayPicker />
			</div>
		)
	};
}

export default Calendar;
