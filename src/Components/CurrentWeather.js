 import React, { Component } from 'react';
 import axios from 'axios';
 import './../public/css/weather-icons.css';
 import './../public/css/weather.min.css';
 import config from './../config.js';
 const apiURL = config.apiURL;
// const apiDomain = config.apiDomain;

const currentTime = new Date().getHours();
 const dayTime = currentTime >= 6 && currentTime <= 19;
 const prefix = 'wi wi-owm-';
 let timeOfDay = 'day-';
 if(!dayTime) {
	 timeOfDay = 'night-';
 }

 class CurrentWeather extends Component {
	 constructor(props) {
		 super(props);
     this.intervalId = null;
		 this.state = {
			 currWeatherLoaded: false,
			 weatherIcon: '',
			 temp: '',
			 weather: 'Loading...'
		 };
	 }

	componentDidMount() {
		 this.refresh();
     this.intervalId = setInterval(this.refresh.bind(this), this.props.refreshInterval * 1000);
	 }

   componentWillUnmount(){
 				clearInterval(this.intervalId);
 	 }

   refresh = () => {
		 navigator.geolocation.getCurrentPosition(this.locationSuccess, this.locationError);
	 }

	 getWeather(pos) {
		 let lat = pos.coords.latitude;
		 let lon = pos.coords.longitude;
		 axios.get(`${apiURL}/weather/current/${lat}/${lon}`)
			 .then(res => {
				 //console.log("curr weather: " + res.data);
         if(res.status === 200) {
           this.setState({
  						currWeatherLoaded: true,
  						weatherIcon: prefix + timeOfDay + res.data.weather.weather[0].id,
  						temp: res.data.weather.main.temp
  				 });
         }
			 });
	 }

	 locationSuccess = (pos) => {
		 this.getWeather(pos);
	 };

	 locationError = (err) => {
		console.log('Curr weather error'  + err);
		this.setState({
			 weather: 'Service temporarily unavailable'
		})
	};

		convertToF = (temp) => {
			return Math.round(1.8 * (temp - 273) + 32);
		}

		convertToC = (temp) => {
			return Math.round(temp - 273.15);
		}

	 render() {

		 let temp;
		 if(this.props.localization === 'us') {
			 temp = this.convertToF(this.state.temp);
		 }
		 else {
			 temp = this.convertToC(this.state.tempC);
		 }

		 return this.state.currWeatherLoaded
			 ?<div className="Weather-wrapper">
				<div id="curr-weather">
					<span className="Weather-temp">
							{temp}<i className={this.props.localization === 'us' ? 'wi wi-fahrenheit' : 'wi wi-celsius'}></i>
					</span>
					<div className="weather-icon">
						<i className={this.state.weatherIcon}></i>
					</div>
			 </div>
			 </div>
			 :<div className="Weather-wrapper">
					{this.state.weather}<i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
			 </div>
	 };
 }

 export default CurrentWeather;
