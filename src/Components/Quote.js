import React, { Component } from 'react';
import axios from 'axios';
import config from '../config.js';

class Quote extends Component {
	constructor(props) {
		super(props);

		this.state = {
            quote: '',
            author: '',
			quoteLoaded: false
		};
	}

	getQuote = () => {
		axios.get(`${config.apiURL}/quote`)
			.then(res => {
                
				this.setState({
                    quote: res.data.quote.quote,
                    author: res.data.quote.author,
					quoteLoaded: true,
				});
			});
	}

	componentDidMount() {
		this.getQuote();
	}

	render() {

		return this.state.quoteLoaded
			?<div id="Quote">
				 <div>
                    "<span>
                        {this.state.quote}
                    </span>"
                    <span>  -{this.state.author}</span>
				 </div>
			</div>
			:<div id="Quote">
				<div>
					Loading...<i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
				</div>
			</div>
	};
}

export default Quote;
