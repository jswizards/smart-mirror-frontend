import React, { Component } from 'react';
import axios from 'axios';
import config from '../config.js';
import FaChevronCicleUp from 'react-icons/lib/fa/chevron-circle-up';
import './../public/css/cryptoPrices.min.css';

class CryptoPrice extends Component {
	constructor(props) {
		super(props);

		this.state = {
            crypto: null,
            cryptoLoaded: false,
            intervalId: null,
		};
	}

	getCryptoPrice(){
		axios.get(`${config.apiURL}/crypto/${this.props.coins}`)
			.then(res => {
                let cryptoData = res.data;

                if(this.state.cryptoLoaded){
                    //We have old prices -> let's calculate the change

                    Object.keys(cryptoData).forEach(coin=>{
                        const oldPrice = this.state.crypto[coin].USD
                        const newPrice = cryptoData[coin].USD

                        if( oldPrice < newPrice){
                            cryptoData[coin].change = 1;
                        }else if(oldPrice > newPrice){
                            cryptoData[coin].change = -1;
                        }else {
                            cryptoData[coin].change = this.state.crypto[coin].change;
                        }
                    })
                    this.setState({
                        crypto: cryptoData,
                        cryptoLoaded: true,
                    });

                }else{
                    //First time loaded
                    this.setState({
                        crypto: cryptoData,
                        cryptoLoaded: true,
                    });
                }




            });
	}

	componentDidMount() {
        //Initial call
        this.getCryptoPrice();

        //Create interval to continually get prices:
        const intervalId = setInterval(this.getCryptoPrice.bind(this), this.props.refreshInterval * 1000);

        //Save intervalId to destroy it after
        this.setState({intervalId: intervalId});
    }

    componentWillUnmount(){
        //Destroy interval
        clearInterval(this.state.intervalId);
    }

	render() {
		return this.state.cryptoLoaded
			?<div id="CryptoPrices">
                <ul>
                    {Object.keys(this.state.crypto).map(cryptoSymbol => {
                        return <li className='coin'key={cryptoSymbol}>
                                {cryptoSymbol} - {this.state.crypto[cryptoSymbol].USD} $
                                <FaChevronCicleUp
                                    className={(this.state.crypto[cryptoSymbol].change < 0) ? 'changeArrow rotate180':'changeArrow'}
                                />
                        </li>
                    })}
                </ul>
			</div>
			:<div id="CryptoPrices">
				<div>
					Loading...<i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
				</div>
			</div>
	};

}

export default CryptoPrice;
