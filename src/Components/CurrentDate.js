import React, { Component } from 'react';
import Moment from 'react-moment';

const date = new Date();

class CurrentDate extends Component {
	render() {
		return <div><Moment format="ddd MMM Do YYYY">{date}</Moment></div>
	}
}

export default CurrentDate;
