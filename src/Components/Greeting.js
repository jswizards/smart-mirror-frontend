import React, { Component } from 'react';


class Greeting extends Component {


	render() {

        return <div>
                {
                    this.props.name
                    ? <span>Hello {this.props.name}</span>
                    :''
                }

            </div>
	};
}

export default Greeting;
