import React, { Component } from 'react';
import axios from 'axios';
import Moment from 'react-moment';
import socketIOClient from 'socket.io-client';
import './../public/css/events.min.css';

import config from '../config.js';
const apiURL = config.apiURL;
const apiDomain = config.apiDomain;

class Events extends Component {
	constructor(props) {
		super(props);
		this.intervalId = null;
		this.state = {
			eventsLoaded: false,
			events: 'Loading...',
			eventsHeader: ''
		};
	}

	getEvents() {
		axios.get(`${apiURL}/google/events`)
			.then(res => {
				//console.log(res);
				//console.log("Error: " + res.data.error);
				if(res.status === 200) {
					this.setState({
						eventsLoaded: true,
						events: res.data.events,
					});
				}
				else {
					this.setState({
						eventsLoaded: false,
						events: 'Seervice temporarily unavailable'
					});
				}
				//this.reduceData(res.data.events);
			});
	}



	componentDidMount() {
		this.getEvents();
		this.intervalId = setInterval(this.getEvents.bind(this), this.props.refreshInterval * 1000);
		const socket = socketIOClient(apiDomain)
		socket.on('update google calendar events', () => {
			console.log("update events emit received!");
			this.getEvents();
		})
	}

	componentWillUnmount(){
		 clearInterval(this.intervalId);
	}

	render() {
		const currentDate = new Date();
		const currentFullDate = `${currentDate.getMonth()}/${currentDate.getDate()}/${currentDate.getFullYear()}`;

		if(!this.state.eventsLoaded) {
			return(
				<div id="Events-wrapper">
					{this.state.events}<i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
				</div>
			)
		}
		else{
			let counter = 0;
			return(this.state.events.length > 0
				?<div id="Events-wrapper">
						<ul>
							{	this.state.events.map(oneEvent => {
								let eventStartDate = new Date(oneEvent.start.dateTime);
								let fullEventStartDate = `${eventStartDate.getMonth()}/${eventStartDate.getDate()}/${eventStartDate.getFullYear()}`;
								if(fullEventStartDate === currentFullDate &&
									 eventStartDate.getTime() > currentDate.getTime() &&
										counter < 5) {
										counter++;
										return(
											<li key={oneEvent.etag}>
												<Moment format="HH:mm ">{oneEvent.start.dateTime}</Moment>
												{oneEvent.summary}
											</li>
										)
								}
								else {
									return(<div/>);
								}
							})
						}
						</ul>
						<hr />
						</div>
						:<div id="Events-wrapper" />
			)
		}
		}
	// reduceData = (events) => {
	// 	let datesSummaries = [];
	// 	//console.log("reduce data called");
	// 	events.forEach(item => {
	// 		// let hours = new Date(item.start.dateTime).getHours();
	// 		// let minutes = new Date(item.start.dateTime).getMinutes();
	// 	datesSummaries.push(
	// 		{date: new Date(item.start.dateTime).getMonth()+1 + "/" +
	// 					 new Date(item.start.dateTime).getDate() + "/" +
	// 					 new Date(item.start.dateTime).getFullYear(),
	// 		 desc: item.summary,
	// 		 fullDate: item.start.dateTime,
	// 		 id: item.id
	// 	 });
	// 	});
	// 	let result = datesSummaries.reduce((r, a) => {
	// 		r[a.date] = r[a.date] || [];
	// 		r[a.date].push({desc: a.desc, fullDate: a.fullDate, id: a.id})
	// 		return r;
	// }, []);
	// //console.log(result);
	//
	// this.setState({
	// 	events: result
	// });
	// }

// 	render() {
// 		return ( this.state.eventsLoaded && Array.isArray(this.state.events)
// 						?<div id="Events">
// 							<span id="Event-header">Upcoming events:</span>
// 							<ul>
// 							{
// 								Object.keys(this.state.events).map((el, index) => {
// 								if (index <= 5) {
// 									return (
// 										<li key={el}>
// 											<div className="Event-date">
// 												<Moment format="ddd MMM Do ">{new Date(el)}</Moment>
// 											</div>
// 											{this.state.events[el].map((oneItem, index) => {
// 												let randomNum = Math.random() * 100;
// 												return(
// 														<div key={oneItem.id} className="Event-desc">
// 																<ul>
// 																	<li key={oneItem.id}>
// 																		<Moment format="HH:mm ">{oneItem.fullDate}</Moment>
// 																		{oneItem.desc}
// 																	</li>
// 																</ul>
// 														</div>
// 												)})}
// 										</li>
// 									)
// 								}})
// }
// 							</ul>
// 						</div>
// 						:<div id="Events"><ul>{this.state.events}
// 							<i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i></ul>
// 						</div>
// 		)
// 	}
}

export default Events;
