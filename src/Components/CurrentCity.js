import React, { Component } from 'react';
import axios from 'axios';
import './../public/css/current-city.min.css';

import config from './../config.js';
const apiURL = config.apiURL;
//const apiDomain = config.apiDomain;

class CurrentCity extends Component {
	constructor(props) {
	 super(props)
	 this.refreshTimeout = 5000;
	 this.state = {
		 locationLoaded: false,
		 city: "Locating..."
	 }
	};

	success = (pos) => {
		let lat = pos.coords.latitude;
		let lon = pos.coords.longitude;
		// console.log(lat);
		// console.log(lon);
		axios.get(`${apiURL}/current-city/${lat}/${lon}`)
			.then(res => {
				//console.log("Current city: " + res.data);
				if(res.status === 200) {
					this.setState({
						locationLoaded: true,
						city: res.data
					});
				}
				else {
					this.refresh();
				}
			});
	};

	error = (err) => {
		console.log("current city failed");
		this.setState({
			city: "Location unavailable"
		});
		this.refresh();
	};

	componentDidMount() {
		navigator.geolocation.getCurrentPosition(this.success, this.error);
	}

	refresh = () => {
		setTimeout(() => {
			navigator.geolocation.getCurrentPosition(this.success, this.error);
		}, this.refreshTimeout);
	}

	render() {
		return !this.state.locationLoaded
					? <div id="CurrentCity-name">
							{this.state.city}<i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
						</div>
					: <div id="CurrentCity-name">
							{this.state.city}
						</div>
	}
}

export default CurrentCity;
