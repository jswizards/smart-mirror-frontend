import React, { Component } from 'react';
import axios from 'axios';
//import Moment from 'react-moment';
//import socketIOClient from 'socket.io-client';
import './../public/css/commute.min.css';
import config from './../config.js';
const apiURL = config.apiURL;
//const apiDomain = config.apiDomain;

class Commute extends Component {
	constructor(props) {
		super(props);
		this.intervalId = null;
		this.state = {
			commuteTime: '',
			commuteTimeTolls: '',
			commuteDistance: '',
			commuteTolls: '',
			commuteCopyright: '',
			commuteDirections: '',
			commuteLoaded: false,
			incidents: [],
			commuteErrors: '',
			trafficErrors: ''
		};
	}

	getCommute = () => {
		const reqData = {
			from: this.props.from,
			to: this.props.to,
			avoids: ''
		};

		axios.post(`${apiURL}/commute`, reqData)
			.then(res => {
				//console.log("Drive time " + JSON.stringify(res.data));
				this.setState({
					commuteTime: 'Commute: ' + res.data.time + 'min',
					commuteDistance: res.data.distance,
					commuteTolls: res.data.hasTolls,
					commuteCopyright: res.data.copyright,
					commuteDirections: res.data.directions,
					commuteLoaded: true,
					commuteErrors: res.data.error
				});
				if(this.state.commuteTolls) {
					reqData.avoids = 'Toll Road';
					axios.post(`${apiURL}/commute`, reqData)
						.then(res => {
							this.setState({
									commuteTimeTolls: '| Avoid tolls: ' + res.data.time + 'min'
							});
						});
				}
				this.getTraffic(res.data.boundingBox);
			});
	}

	getTraffic = (boundingBox) => {
		if(boundingBox) {
			const reqData = {
				boundingBox: `${boundingBox.ul.lat},${boundingBox.ul.lng},${boundingBox.lr.lat},${boundingBox.lr.lng}`,
				filters: this.props.trafficFilters
			};
			//console.log("Bounding box: " + reqData.boundingBox);
			//console.log("filters: " + reqData.filters);
			axios.post(`${apiURL}/commute/traffic`, reqData)
				.then(res => {
					//console.log("traffic: " + JSON.stringify(res.data.incidents));
					this.setState({
						incidents: res.data.incidents,
						trafficErrors: res.data.error
					});
				});
		}
	}

	componentDidMount() {
		this.getCommute();
		this.intervalId = setInterval(this.getCommute.bind(this), this.props.refreshInterval * 1000);
	}

	componentWillUnmount(){
		clearInterval(this.intervalId);
	 }

	render() {

		return this.state.commuteLoaded
			?<div className="Commute-wrapper">
				 <div id="Commute-time">
						<img src={this.state.commuteCopyright.imageUrl} alt="MapQuest" />
					 <i className="fas fa-car"></i>
						{this.state.commuteTimeTolls}
						{this.state.commuteTime}
				 </div>
				 <div id="Commute-alerts">
					 <ul>
						 {this.state.incidents.map(oneIncident => {
							if(oneIncident.impacting) {
								return(
									<li key={oneIncident.id}>
										<img src={oneIncident.iconURL} alt="alert"/>
										{oneIncident.shortDesc}
									</li>
								)
							}
							else {
								return('')
							}
						})
					}
					</ul>
				 </div>
			</div>
			:<div className="Commute-wrapper">
				<div id="Commute-time">
					Loading...<i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
				</div>
			</div>
	};
}

export default Commute;
