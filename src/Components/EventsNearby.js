import React, { Component } from 'react';
import axios from 'axios';
import Moment from 'react-moment';
//import socketIOClient from 'socket.io-client';
import './../public/css/events-nearby.min.css';
import config from './../config.js';
const apiURL = config.apiURL;
//const apiDomain = config.apiDomain;

class EventsNearby extends Component {
	constructor(props) {
		super(props);
		this.intervalId = null;
		this.state = {
			eventsLoaded: false,
			events: "Loading..."
		};
	}

	getEvents(pos) {
		let reqData = {
			range: this.props.range,
			units: (this.props.localization) === "us" ? "mi" : "km",
			lat: pos.coords.latitude,
			lon: pos.coords.longitude,
			sortBy: 'date'
		}
		axios.post(`${apiURL}/events-nearby`, reqData)
			.then(res => {
				//console.log("Events nearby " + res.data.events);
				this.setState({
					eventsLoaded: true,
					events: res.data.events,
				});
			});
	}

	locationSuccess = (pos) => {
		this.getEvents(pos);
	};

	locationError = (err) => {
		console.log('Events nearby error ' + err);
		this.setState({
			events: "Service temporarily unavailable"
		})
	};

	componentDidMount() {
		this.refresh();
		this.intervalId = setInterval(this.refresh.bind(this), this.props.refreshInterval * 1000);
	}

	componentWillUnmount(){
				clearInterval(this.intervalId);
	 }

	 refresh = () => {
		 navigator.geolocation.getCurrentPosition(this.locationSuccess, this.locationError);
	 }

	render() {
		let displayedEvents = [];
		if(!this.state.eventsLoaded) {
			return(
				<div id="Events-nearby-wrapper">
					{this.state.events}<i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
				</div>
			)
		}
		else{
			return(this.state.events.length > 0
				?<div id="Events-nearby-wrapper">
						<span className="Events-nearby-header">Check out these events in your area:</span>
						<ul>
							{this.state.events.map((oneEvent, index) => {
									let admission = (oneEvent.isFree) ? '' : '$ ';
									let displayedLen = displayedEvents.length;
									let duplicate = displayedEvents.indexOf(oneEvent.name.text) > -1;
									//show only first 5 events and no duplicate titles!
									if(displayedLen <= 4 && !duplicate) {
										displayedEvents.push(oneEvent.name.text);
										return(
											<li key={oneEvent.id}>
												<Moment format="HH:mm ">{oneEvent.start.local}</Moment>
												{admission}
												{oneEvent.name.text}
											</li>
										)
									}
									else {
										return('')
									}
							})
						}
						</ul>
						</div>
						:<div id="Events-nearby-wrapper" />
			)
		}
	}
}

export default EventsNearby;
