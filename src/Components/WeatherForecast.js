import React, { Component } from 'react';
import axios from 'axios';
import './../public/css/weather-icons.css';
import './../public/css/weather.min.css';
import config from './../config.js';
const apiURL = config.apiURL;

const currentTime = new Date().getHours();
const newDate = new Date();
let nextDate = newDate;
nextDate.setDate(nextDate.getDate() + 1);
nextDate = nextDate.getDate();

const dayTime = currentTime >= 6 && currentTime <= 19;
const prefix = 'wi wi-owm-';
let timeOfDay = 'day-';
if(!dayTime) {
	timeOfDay = 'night-';
}

class WeatherForecast extends Component {
	constructor(props) {
		super(props);
		this.intervalId = null;
		this.state = {
			forecastLoaded: false,
			forecast: "Loading..."
		};
	}

	componentDidMount() {
		 this.refresh();
		 this.intervalId = setInterval(this.refresh.bind(this), this.props.refreshInterval * 1000);
	 }

	 componentWillUnmount() {
		 clearInterval(this.intervalId);
	 }

	 refresh = () => {
		 navigator.geolocation.getCurrentPosition(this.locationSuccess, this.locationError);
	 }

	 getForecast(pos) {
		 let lat = pos.coords.latitude;
		 let lon = pos.coords.longitude;
		 axios.get(`${apiURL}/weather/forecast/${lat}/${lon}`)
			 .then(res => {
				 //console.log("forecast: " + res.data);
				 this.setState({
					 forecastLoaded: true,
					 forecast: res.data.forecast
				 });
			 });
	 }

	 locationSuccess = (pos) => {
		 this.getForecast(pos);
	 };

	 locationError = (err) => {
		console.log('Weather forecast error'  + err);
		this.setState({
			forecast: 'Service temporarily unavailable'
		})
	};

	 convertToF = (temp) => {
		 return Math.round(1.8 * (temp - 273) + 32);
	 }

	 convertToC = (temp) => {
		 return Math.round(temp - 273.15);
	 }

	render() {
		// <i className="wi wi-thermometer"></i>
		// <span id="day"><Moment format="dddd">{oneDate.dt_txt}</Moment></span>
		return this.state.forecastLoaded
			?<div className="Weather-wrapper">
			<h4>Tomorrow</h4>
			<ul>
			{this.state.forecast.map(oneDate => {
			let weatherCode = oneDate.weather[0].id;
			let weatherIcon = prefix + timeOfDay + weatherCode;
			let temp;
				if(this.props.localization === "us") {
					temp = this.convertToF(oneDate.main.temp);
				}
				else {
					temp = this.convertToC(oneDate.main.temp);
				}
			let timeFromApi = new Date(oneDate.dt_txt).getHours();
			let dateFromApi = new Date(oneDate.dt_txt).getDate()
			let withinRange = dateFromApi === nextDate;
			// if(currentTime > 21) {
			// 	withinRange = dateFromApi === nextDate;
			// }
			// else {
			// 	withinRange = dateFromApi === currentDate && timeFromApi > currentTime;
			// 	//console.log("dateFromApi: " + dateFromApi + " current date: " + currentDate + " timeFromApi: " + timeFromApi + "currentTime: " + currentTime);
			// }
			if(withinRange) {
				return(
					<li key={oneDate.dt}>
						{timeFromApi <= 9 ? timeFromApi = "0" + timeFromApi : timeFromApi}:00
						<div className="weather-icon"><i className={weatherIcon}></i></div>
						<span className="Weather-temp">
							{temp}<i className={this.props.localization === "us" ? "wi wi-fahrenheit" : "wi wi-celsius"}></i>
						</span>
					</li>
				)
			}
			else {
				return('')
			}
			})
			}
			</ul>
			</div>

			:<div className="Weather-wrapper">
			{this.state.forecast}<i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
			</div>
	};
}

export default WeatherForecast;
