import React, { Component } from 'react';
import './App.css';
import './public/css/fontawesome-all.min.css';
import config from './config.js';
import axios from 'axios';
//import Calendar from './Components/Calendar';
import WeatherForecast from './Components/WeatherForecast';
import Greeting from './Components/Greeting';
import CurrentWeather from './Components/CurrentWeather';
import Clock from 'react-live-clock';
import CurrentCity from './Components/CurrentCity';
import Events from './Components/Events';
import CurrentDate from './Components/CurrentDate';
import Commute from './Components/Commute';
import News from './Components/News';
import EventsNearby from './Components/EventsNearby';
import AlarmClock from './Components/AlarmClock';
import Quote from './Components/Quote';
import CryptoPrice from './Components/CryptoPrice';
import socketIOClient from 'socket.io-client';
import Modal from 'react-responsive-modal';
//import voice from 'responsivevoice';
//import CurrentDate from './Components/CurrentDate';
//import Calendar from 'react-calendar-simple';

// onCreate={this.handleScriptCreate.bind(this)}
// onError={this.handleScriptError.bind(this)}

const apiDomain = config.apiDomain;
const apiURL = config.apiURL;
const defaultShowComponents = {
	currentWeather: true,
	currentCity: true,
	commute: true,
	weatherForecast: true,
	quote: true,
	cryptoPrice: true,
	events: true,
	eventsNearby: true,
	alarmClock: true,
	news: true
}

class App extends Component {

	constructor(props){
		super(props)
		this.state = {
			greetingModal: false,
			user: null,
			showComponents: defaultShowComponents
		}
	}

	// setRef = (webcam) => {
	// 	this.webcam = webcam;
	// }

	// capture = () => {
	// 	const imageSrc = this.webcam.getScreenshot();
	// 	return imageSrc;
	// 	//console.log(imageSrc);
	// }

	// recognizeFace = () => {
	// 	return new Promise(resolve => {
	// 		let reqData = {
	// 			img: this.capture(),
	// 			galleryName: 'SmartMirror',
	// 			action: 'recognize'
	// 		};
	// 		console.log(reqData.img)
	// 		axios.post(`${apiURL}/face-recognition/identity`, reqData)
	// 			.then(res => {
	// 				console.log('KAIROS : ' + JSON.stringify(res))
	// 				// console.log("Kairos recognized following users: " + res.images[0].candidates);
	// 				resolve(res.data.response);
	// 		});
	// 	});
	// }
	loadUserBySubjectId(subjectId){
		axios.get(`${apiURL}/user/subject-id/${subjectId}`)
			.then(res => {
				if(res.status === 200){
					this.updateUser(res.data.user);
				}
		})

   }
	componentDidMount() {
		//TEMP load user
		// this.loadUserBySubjectId('olehkolinko')

		//Create socket connection:
		const socket = socketIOClient(apiDomain);

		//listen for socket events
		socket.on('RASPBERRY:MOTION_DETECTED', ()=>this.setState({greetingModal:true}))
		socket.on('RASPBERRY:PERSON_DETECTED', payload => this.newPerson(payload))
		socket.on('RASPBERRY:PERSON_LEFT', payload => this.personLeft(payload))

	}

	newPerson(payload) { //New person in front of the mirror
		this.updateUser(payload.user);
		this.closeGreetingModal();
	}

	personLeft(payload) { //Nobody is here
		this.setState({
			user: null,
			showComponents: defaultShowComponents
		});
		this.closeGreetingModal();
	}

	updateUser(user){ //New user in fornt of the mirror
		//need to map newShowComponents
		let newShowComponents = {}; //hashMap of components
		Object.keys(this.state.showComponents).forEach( key => { //Map through all keys
			newShowComponents[key] = user.settings[key].show; //save new bool values in hash map
		})

		this.setState({
			user: user, //update user
			showComponents: newShowComponents //update state with new options
		})
	}
	closeGreetingModal(){
		this.setState({greetingModal:false})
	}

	render() {

		return (
			<div className="App">
				 <Modal open={this.state.greetingModal}
				 	styles={{modal: {'width': '30%','text-align': 'center'}}}
					onClose={()=>this.closeGreetingModal()}
					center closeIconSize={0}>
					<h2>
						HELLO
					</h2>
				</Modal>
				<header className="App-header" />
				<div className="App-body">
					<div id="App-left-side">
						<CurrentDate />
						<Clock
							format={'hh:mm:ss A'}
							ticking={true}
							timezone={'US/Eastern'}
							className="App-clock" />

						{
							this.state.showComponents.currentCity
							?<CurrentCity />
							:''
						}

						{
							this.state.showComponents.currentWeather
							?<CurrentWeather
								localization="us"
								refreshInterval="300"
								/>
							:''
						}

						{
							this.state.showComponents.commute
							?<Commute
								from="Boca Raton"
								to="Fort Lauderdale"
								trafficFilters="incidents,construction,congestion"
								refreshInterval="120"
							/>
							:''
						}


						<hr />

						{
							this.state.showComponents.weatherForecast
							?<WeatherForecast
									localization="us"
									refreshInterval="300"
								/>
							:''
						}

						{
							this.state.showComponents.quote
							?<Quote />
							:''
						}
						{
							this.state.showComponents.cryptoPrice
							?<CryptoPrice coins="BTC,ETH,WAX" refreshInterval='10'/>
							:''
						}
					</div>
					<div id="App-center">
						{
							this.state.user
							?<Greeting name={this.state.user.firstName}/>
							: ''
						}

					</div>
					<div id="App-right-side">
						{
							this.state.showComponents.alarmClock
							?<AlarmClock
									kairosSubjectID="ihorbond"
									kairosGalleryName="SmartMirror"
								/>
							:''
						}

						{
							this.state.showComponents.events
							?<Events
								refreshInterval="120"
							/>
							:''
						}

						{
							this.state.showComponents.eventsNearby
							?<EventsNearby
								range="10"
							  localization="us"
								refreshInterval="300"
								/>
							:''
						}

							<hr />

						{
							this.state.showComponents.news
							?<News
								sources=""
								keywords="bitcoin"
								category="business"
								language="en"
								country="us"
								refreshInterval="300"
							/>
							:''
						}

					</div>

				</div>

			</div>
		);
	}
}


export default App;
