export default {
    apiURL: 'http://localhost:4200/rest',
    apiDomain: 'http://localhost:4200',
    streamURL: 'http://localhost:8000/stream.mjpg'
    //apiURL: 'net.tcp://10.0.1.8/rest'
};
